<?php

class Stats extends \ActiveRecord\Model {

    static $connection = 'parser';

    public function getStats() {
        return Stats::find('all');
    }

}
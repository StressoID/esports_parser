<?php

class Urls extends \ActiveRecord\Model {

    static $connection = 'parser';

    public function getActiveUrls() {
        return Urls::find('all', ['active' => 'Y']);
    }
}
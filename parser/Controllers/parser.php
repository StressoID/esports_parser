<?php

class Parser
{

    private $debug = true;

    /** Инициализрует работу парсера.
     * @param $stepCount - (integer) количество страниц, на которые зайдет парсер.
     *
    */

    public function allContent($stepCount = 3)
    {
        $urls = new Urls();
        $activeUrls = $urls->getActiveUrls();
        foreach ($activeUrls as $key => $obj) {
            if (empty($obj->pagination)) {
                $this->getContent($obj);
            } else {
                $this->withPagination($obj, $stepCount);
            }
        }
    }

    /** Добавляет возможность спарсить несколько страниц, которые доступны по пагинации.
     * @param $obj - (Object) объект записи из базы с url и тегами для парсера.
     * @param $stepCount - (integer) кольчество страниц пагинации, по которым пройдется парсер.
    */
    public function withPagination($obj, $stepCount = 2)
    {
        $paging = explode('=', $obj->pagination); //Обрабатываем строку с GET параметром пагинации из базы
        $step = array_pop($paging); // отрезаем и кладем в переменную шаг пагинации
        for ($i = $step; $i <= $step * $stepCount; $i += $step) {
            $pagin = $paging[0] . '=' . $i;
            $this->getContent($obj, $pagin);
        }
    }

    /** Забирает контент с сайта по тегам, которые были переданы в объекте урлов.
     * @param $obj - (Object) объект записи из базы с url и тегами для парсера.
     * @param $pagin - (string) GET параметр части урл, который отвечает за пагинации на целевом сайте.
    */
    public function getContent($obj, $pagin = '')
    {
        $link = 'http://' . $obj->url;
        if ($pagin != '') {
            $link .= $pagin;
        }
        if ($this->debug) {
            echo $link . PHP_EOL;
        }
        $curlConnect = new connect();
        $out = $curlConnect->curl_connect($link);
        $domHtml = str_get_html($out);
        $i = 0;
        foreach ($domHtml->find($obj->begin_tag_anons) as $item) {
            $esports_news = new prevNews();
            $code = $this->codeFromUrl($item->find($obj->href_tag, 0)->href);
            $cur_news = prevNews::find('all', ['code' => $code]);
            if (empty($cur_news)) {
                $esports_news->code = trim($code, '_');
                $esports_news->name = $item->find($obj->title_tag, 0)->innertext;
                $esports_news->href = $item->find($obj->href_tag, 0)->href;
                $esports_news->text = $item->find($obj->text_tag, 0)->innertext;
                $esports_news->img = $item->find($obj->href_tag.' img', 0)->src;
                $esports_news->tags_id = 1;
                $esports_news->site_name = $obj->site_name;
                if ($esports_news->save()) {
                    $i++;
                }

                if (strpos($item->find($obj->href_tag, 0)->href, 'http://www.') !== false) {
                    $url = str_replace('http://','',$item->find($obj->href_tag, 0)->href);
                } elseif (strpos($item->find($obj->href_tag, 0)->href, 'http://') !== false) {
                    $url = $item->find($obj->href_tag, 0)->href;
                } else {
                    $url = 'http://'.$obj->site_name.$item->find($obj->href_tag, 0)->href;
                }

                $this->saveDetail($url, $obj, $esports_news->id, $esports_news->code);

                if ($this->debug) {
                    echo 'News added' . PHP_EOL;
                }
            } else {
                if ($this->debug) {
                    echo 'item exist' . PHP_EOL;
                }
            }
        }
        $stats = new Stats();
        $stats->status = 'Загружено '.$i;
        $stats->urls_id = $obj->id;
        $stats->save();
        $domHtml->clear();// подчищаем за собой
        unset($data);

    }

    public function codeFromUrl($url)
    {
        $url = preg_replace('/http:\/\/(www.)?/', '', $url);
        $url = preg_replace('/[-.\/]/','_', $url);
        $url = preg_replace('/_html$/','', $url);
        return $url;
    }

    public function saveDetail($url, $obj, $prev_id, $code) {
        $curlConnectDetail = new connect();
        $out = $curlConnectDetail->curl_connect($url);
        $domHtml = str_get_html($out);
        foreach ($domHtml->find($obj->detail_tag) as $item) {
            $detailNews = new detailNews();
            $detailNews->prev_id = $prev_id;
            $detailNews->code = $code;
            $detailNews->title = $item->find($obj->detail_title, 0)->innertext;
            if (!empty($obj->delete_tag)) {
                $item->find($obj->delete_tag, 0)->outertext=''; //Удаляем ненужный див
            }
            $detailNews->text = $item->find($obj->detail_text, 0)->innertext;
            $detailNews->save();
            if ($this->debug) {
                echo 'Detail news added'.PHP_EOL;
            }
        }
        $domHtml->clear();// подчищаем за собой
        unset($data);
    }

}
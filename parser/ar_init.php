<?php
$connections = array(
    'project' => 'mysql://root:@127.0.0.1/juekrtn3_esports;charset=utf8',
    'parser' => 'mysql://root:@127.0.0.1/esports_parsers;charset=utf8'
);

ActiveRecord\Config::initialize(function($cfg) use ($connections)
{
    $cfg->set_model_directory('.');
    $cfg->set_connections($connections);
});

function deb($arr) {
    echo '<pre>',print_r($arr,1),'</pre>';
}
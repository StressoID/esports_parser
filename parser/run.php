<?php
require_once("../vendor/autoload.php");
require_once('ar_init.php');
require_once('Models/Urls.php');
require_once('Models/News.php');
require_once('Models/detailNews.php');
require_once('Models/Stats.php');
require_once('connect.php');
require_once('../simp/simple_html_dom.php');
require_once('Controllers/parser.php');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$parser = new Parser();
$parser->allContent(1);
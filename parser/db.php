<?php

class Urls extends \ActiveRecord\Model {
    public function getActiveUrls() {
        return Urls::find('all', ['active' => 'Y']);
    }
}
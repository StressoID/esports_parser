<?php

class Connect {
    private $connection;
    private $code_html;

    public function curl_connect($url)
    {
        $this->connection = curl_init();
        curl_setopt($this->connection, CURLOPT_URL, $url);
        curl_setopt($this->connection, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36');
        curl_setopt($this->connection, CURLOPT_HEADER, 1);
        curl_setopt($this->connection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->connection, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($this->connection, CURLOPT_COOKIEFILE, 'cookie.txt');
        curl_setopt($this->connection, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->connection, CURLOPT_FOLLOWLOCATION, true);
        $this->code_html = curl_exec($this->connection);
        curl_close($this->connection);
        var_dump($this->code_html);
    }
}